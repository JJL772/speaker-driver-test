/*
 *
 *
 * Main procedure for project
 *
 *
 */ 
#include <xc.h>

short int volume_mod;

#define RC3_BIT (1 << 3)
#define RC2_BIT (1 << 2)
#define RA2_BIT (1 << 2)
#define RA0_BIT 1

int main()
{
	//Clear Ports
	PORTA = 0x0;
	PORTC = 0x0;
	TRISA = 0x0;
	TRISC = 0x0;
	LATA  = 0x0;
	LATC  = 0x0;
	ANSELA = 0x0;
	ANSELC = 0x0;

	//Configure ports for inputs
	TRISC &= RC3_BIT;
	TRISA &= RA2_BIT;

	//Set RA0 to output analog sinals
	ANSELA &= RA0_BIT;

	//Enable external interrupts
	PIE0 = 0x1;

	//Enable interrupts on the pin RC3
	INTPPS = 0x13;

	//Configure the oscillator to use the low power internal oscillator (runs at 31kHz) and set the clock divider to 1
	OSCCON1 = 0x50;

	//Loop and wait for the new clock to stabilize and take effect
	//for(short i = 256; i > 0; i--);

	//Configure the DAC to output an analong signal on RA0
	DAC1CON0 = 0xA0;

	//Enable interrupts
	INTCON &= (1<<7);

	while(1)
	{
		//Now generate the clock
		PORTC ^= RC2_BIT; //1 cycle latency
		PORTC = PORTC; //Another clock
		//The C compiler might turn this into a different segment of assembly, so it might come out a bit off
	}
}

//ISR for change on RB7
//Written in C because assembly documentation for XC8 is pretty poor, and doesnt tell me how to specify an interrupt.
void __interrupt(high_priority) btn1Int(void)
{
        if((PIR0 & 0x1) == 1)
        {
                if((PORTA & (1<<2)) == 1)
                {
                        if(volume_mod > 0)
                                volume_mod--;
                        return;
                }

                if(volume_mod < 3)
                        volume_mod++;
                PIR0 = 0x0;
        }
}
