# Speaker-Driver-Test

This is a small PIC microcontroller-based project that drives a speaker so it produces a single tone.
Initially I wrote the firmware in assembly, however microchip's assembler for PIC microcontrollers seems to refuse to generate a proper main function.
Due to this, I ported the code to C (where it's actually much more compact), however the assembly sources are still included with the project.
The code and the circuit were built earlier this month (Start of January).

The microcontroller used in this project was the PIC16(L)F15325, which is an 8-bit microcontroller with a max clock speed of 32MHz.

![Circuit schematic](schematic.png)
The circuit is pretty simple, only composed of several capacitors, resistors and a single transistor.
C1 is a decoupling capacitor for low frequency noise and C2 is a capacitor for high frequency noise.
The transistor used is actually not really meant for driving a speaker; it's an N-channel MOSFET built for power applications.

Initially, I was going to generate a rough sine wave using the microcontroller's digital to analog converter, but I decided against that because the current only begins to flow through the transistor
with a gate voltage of around 4 volts, about 1 volt lower than the 5 volts the circuit was going to be driven with.

Instead, a simple while loop is used to toggle the pin RC2 on and off at a frequency of around 16kHz, generating a super fancy square wave.

In the circuit there are 2 buttons for changing the volume of the speaker, but since the DAC isn't used to generate the signal anymore, the volume pins are unused.

![Circuit](circuit.jpg)

This is the final product on a breadboard, the speaker isn't connected here.
Most of the wires on the board are actually used as jumpers.

After testing the circuit, I found out that there isn't actually isn't any gain over the transistor. 
Rather, the microcontroller doesn't have enough voltage to properly drive the transistor, so the speaker emit a super quiet tone.

I ended up replacing the power transistor with a BJT I pulled off of a radio controller's board. Luckily the microcontroller was able to properly drive the this, producing a much louder tone.

The final circuit looked like this:

![Final Circuit](circuit-final.jpg)

Note: The chip programmer mount and jumpers were removed in the last picture.